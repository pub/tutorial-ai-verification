import numpy as np


class NNetLoader:
    """Class used to load NNet models.

    Code copied and slightly modified from actual NNet implementation.
    See: https://github.com/sisl/NNet/blob/master/python/nnet.py#L3

    Attributes
        ----------
        numLayers: [int]
            Number of weight matrices or bias vectors
            in neural network
        layerSizes: [list[int]]
            Size of input layer, hidden layers, and output layer
        inputSize: [int]
            Size of input
        outputSize: [int]
            Size of output
        mins: [list[float]]
            Minimum values of inputs
        maxes: [list[float]]
            Maximum values of inputs
        means: [list[float]]
            Means of inputs and mean of outputs
        ranges: [list[float]]
            Ranges of inputs and range of outputs
        weights: [list[np.array]]
            Weight matrices in network
        biases: [list[np.array]]
            Bias vectors in network
    """

    def __init__(self, file_path):
        """Read .nnet files as models.

        Parameters
        ----------
        file_path: [str]
            Path to valid .nnet file.
        """
        with open(file_path) as f:
            line = f.readline()
            count = 1

            while line[0:2] == "//":
                line = f.readline()
                count += 1

            # numLayer doesn't include the input layer
            numLayers, inputSize, outputSize, _ = [int(x) for x in line.strip().split(",")[:-1]]

            line = f.readline()

            layerSizes = [int(x) for x in line.strip().split(",")[:-1]]

            line = f.readline()
            # symmetric = int(line.strip().split(",")[0])

            line = f.readline()
            inputMinimums = [float(x) for x in line.strip().split(",")[:-1]]

            line = f.readline()
            inputMaximums = [float(x) for x in line.strip().split(",")[:-1]]

            line = f.readline()
            inputMeans = [float(x) for x in line.strip().split(",")[:-1]]

            line = f.readline()
            inputRanges = [float(x) for x in line.strip().split(",")[:-1]]

            weights = []
            biases = []
            for layernum in range(numLayers):
                previousLayerSize = layerSizes[layernum]
                currentLayerSize = layerSizes[layernum + 1]
                weights.append([])
                biases.append([])
                weights[layernum] = np.zeros((currentLayerSize, previousLayerSize))
                for i in range(currentLayerSize):
                    line = f.readline()
                    aux = [float(x) for x in line.strip().split(",")[:-1]]
                    for j in range(previousLayerSize):
                        weights[layernum][i, j] = aux[j]
                # biases
                biases[layernum] = np.zeros(currentLayerSize)
                for i in range(currentLayerSize):
                    line = f.readline()
                    x = float(line.strip().split(",")[0])
                    biases[layernum][i] = x

        self.numLayers = numLayers
        self.layerSizes = layerSizes
        self.inputSize = inputSize
        self.outputSize = outputSize
        self.mins = inputMinimums
        self.maxes = inputMaximums
        self.means = inputMeans
        self.ranges = inputRanges
        self.weights = weights
        self.biases = biases

    def run_NNet(self, inputs):
        """
        Parses .nnet file and execute it on the input.

        Args:
            file_path: path to NNet file
            inputs: input array as np.array

        Returns:
            NNet model prediction
        """
        assert isinstance(inputs, np.ndarray), "Input must be a numpy array"
        assert self.inputSize == len(inputs), f"Input size must match model input size: {len(inputs)} != {self.inputSize}"
        input = inputs.copy()
        # rescaling inputs
        for i in range(self.inputSize):
            input[i] = (input[i] - self.means[i]) / self.ranges[i]

        for i in range(self.numLayers):
            weight = np.array(self.weights[i], dtype=np.float64).T
            bias = np.array(self.biases[i], dtype=np.float64)

            input = input @ weight + bias

            # Applying ReLU on model
            if i != self.numLayers - 1:
                if type(input[0]).__name__ == "Interval":
                    for i in range(len(input)):
                        input[i] = type(input[i])(max(input[i].lower, 0.0), max(input[i].upper, 0.0))
                elif type(input[0]).__name__ == "Zonotope":
                    for i in range(len(input)):
                        input[i] = input[i].relu()
                else:
                    input = np.maximum(input, 0.0)

        # rescaling output
        output = np.zeros(input.shape, dtype=input.dtype)
        for i in range(self.outputSize):
            output[i] = input[i] * self.ranges[-1] + self.means[-1]
        return output
