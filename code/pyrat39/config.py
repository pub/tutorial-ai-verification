"""
global variables defining useful constants e.g. project paths
Usage:
always keep at root folder, update paths if declared dir are moved
"""
from pathlib import Path

PROJ_ROOT = Path(__file__).parent

MODELS_DIR = PROJ_ROOT / "data/models"
DATASET_DIR = PROJ_ROOT / "data/datasets"
PROP_DIR = PROJ_ROOT / "data/properties"
ROUNDING_LIB_DIR = PROJ_ROOT / "src/c_compilation"
TESTS_DIR = PROJ_ROOT / "tests"
TESTS_PROPS = TESTS_DIR / "properties"

CONV_MAX_CAPACITY = 8 * 1_048_576  # 8 Mo

# ref for ACAS Networks: https://arxiv.org/pdf/1810.04240.pdf
# could be used for prettier printing, not very important
ACAS_OUTPUTS = {"y0": "COC", "y1": "WL", "y2": "WR", "y3": "SL", "y4": "SR"}
ACAS_INPUTS = {"x0": "rho", "x1": "theta", "x2": "psi", "x3": "v_own", "x4": "v_int", "x5": "tau", "x6": "a_prev"}
