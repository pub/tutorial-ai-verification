{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Formal verification of deep neural networks: a tutorial\n",
    "\n",
    "The aim of this tutorial is to give a glimpse on the practical side of Formal Verification for Deep Neural Networks.\n",
    "This tutorial is divided in three part:\n",
    "1. Verification by hand\n",
    "2. Real use case application\n",
    "3. Image classification\n",
    "\n",
    "The tutorial material was written by Augustin Lemesle (CEA List) with material provided by Serge Durand (CEA List)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 1: Verification by hand\n",
    "\n",
    "This first part aims to give a rough overview of the challenges posed by the verification of a neural network. In the first part of the lesson you should have seen a technique called Abstract Interpretation which can leverages intervals to estimate the output of a network. You should have done an example by hand of this method. In this part, we will developp a small Python class that will calculate the output automatically with intervals.\n",
    "\n",
    "### Step 1: Encode the network\n",
    "\n",
    "![image](imgs/network.png)\n",
    "\n",
    "With the above network create a function `network(x1, x2)` which reproduces its comportement. It must pass the tests created in `test_network`. For the relu layer, its function is already implemented here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def relu(x):\n",
    "    return max(0, x)\n",
    "\n",
    "def network(x1, x2):\n",
    "    raise NotImplementedError"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def test_network():\n",
    "    assert network(0, 0) == -2.5\n",
    "    assert network(0, -1) == -3\n",
    "    assert network(0, 1) == -1\n",
    "    assert network(-1, 1) == -1\n",
    "    assert network(-1, 0) == -2\n",
    "    assert network(-1, -1) == -3\n",
    "    assert network(1, 0) == -1.5\n",
    "    assert network(1, -1) == -2\n",
    "    assert network(1, 1) == -1\n",
    "    print(\"All tests are working, congratulations!\")\n",
    "    \n",
    "test_network()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 2: Create an Interval\n",
    "\n",
    "Following the rules of interval arithmetic write a class representing an Interval by overriding Python operators. A skeleton is avalaible below.\n",
    "\n",
    "Intervals rules:\n",
    "- $[l, u] + \\lambda = [l + \\lambda, u + \\lambda]$\n",
    "- $[l, u] + [l', u'] = [l + l', u + u']$\n",
    "- $-[l, u] = [-u, -l]$\n",
    "- $[l, u] - [l', u'] = [l - u', u - l']$\n",
    "- $[l, u] * \\lambda =$\n",
    "  - si $\\lambda >= 0$ -> $[\\lambda * l, \\lambda * u]$\n",
    "  - si $\\lambda < 0$ -> $[\\lambda * u, \\lambda * l]$\n",
    "  \n",
    "We will also need to update the relu for it to work on intervals.\n",
    "\n",
    "Some tests are available for you to check if the class implementation is correct."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Interval:\n",
    "    def __init__(self, lower, upper):\n",
    "        assert lower <= upper, \"Upper should always be greater than lower\"\n",
    "        self.lower = lower\n",
    "        self.upper = upper\n",
    "    \n",
    "    def __neg__(self) -> Interval:\n",
    "        return Interval(-self.upper, -self.lower)\n",
    "        \n",
    "    def __add__(self, other) -> Interval:\n",
    "        raise NotImplementedError\n",
    "    \n",
    "    def __radd__(self, other) -> Interval:\n",
    "        return self.__add__(other)\n",
    "        \n",
    "    def __sub__(self, other) -> Interval:\n",
    "        raise NotImplementedError\n",
    "    \n",
    "    def __mul__(self, other) -> Interval:\n",
    "        if isinstance(other, Interval):\n",
    "            raise TypeError\n",
    "        raise NotImplementedError\n",
    "    \n",
    "    def __rmul__(self, other) -> Interval:\n",
    "        return self.__mul__(other)\n",
    "\n",
    "    def __truediv__(self, other) -> Interval:\n",
    "        if isinstance(other, Interval):\n",
    "            raise TypeError\n",
    "        return self.__mul__(1 / other)\n",
    "        \n",
    "    def __str__(self) -> str:\n",
    "        return f\"Interval({self.lower}, {self.upper})\"\n",
    "\n",
    "    def __repr__(self) -> str:\n",
    "        return self.__str__()\n",
    "\n",
    "    def __eq__(self, other) -> bool:\n",
    "        return self.lower == other.lower and self.upper == other.upper"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def relu(x):\n",
    "    if isinstance(x, Interval):\n",
    "        raise NotImplementedError\n",
    "    else:\n",
    "        return max(0, x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def test_interval():\n",
    "    assert Interval(0, 1) == Interval(0, 1)\n",
    "    assert -Interval(0, 1) == Interval(-1, 0)\n",
    "    assert Interval(0, 1) + 3 == Interval(3, 4)\n",
    "    assert Interval(0, 1) + Interval(1, 2) == Interval(1, 3)\n",
    "    assert Interval(0, 1) - Interval(3, 4) == Interval(-4, -2)\n",
    "    assert Interval(-1, 2) * 3 == Interval(-3, 6)\n",
    "    assert Interval(-1, 2) * -3 == Interval(-6, 3)\n",
    "    assert relu(Interval(-2, 3)) == Interval(0, 3)\n",
    "    print(\"Your Intervals are working!\")\n",
    "    \n",
    "test_interval()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 3: Run the network with intervals\n",
    "\n",
    "At this point you should be able to run the network using the interval class and see the output reached.\n",
    "\n",
    "If you obtain a different results check whether your are modifying the object when doing an addition. $x_1 + x_2$ should return a new Interval and not modify $x_1$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "assert network(Interval(-1, 1), Interval(-1, 1)) == Interval(-7, 5)\n",
    "print(\"You finished the first part!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*****\n",
    "\n",
    "## Part 2: A real problem ACAS-Xu\n",
    "\n",
    "Let's tackle a more complex network of the public benchmark of Airborne Collision Avoidance System for Unmanned vehicles (ACAS-Xu). For an introduction of the ACAS-Xu benchmark, see _An Introduction to ACAS Xu and the Challenges Ahead , Manfredi G., Jestin Y._\n",
    "\n",
    "Formerly, programs such as ACAS were implemented as a lookup table. The neural network approach was partly introduced to reduce the memory requirements for hardware of the original 2GB tables down to 150MB. Neural network becoming more and more efficient, industrials considered using them as a possible replacement. Since we are dealing with critical systems, software safety is a major concern.\n",
    "\n",
    "In _Reluplex: An Efficient SMT Solver for Verifying Deep Neural Networks_ , Katz et al. provided a neural network implementation of a ACAS-Xu system, as well as a tooling to formally verify several safety properties. It was then adopted as a common benchmark in the literature.\n",
    "\n",
    "The problem is the following: a neural network needs to give a direction advisory to a drone when an intruder is detected in its proximity. The advisory can be \"Clear of Conflict\" meaning there is no risk of collision, \"Right\", \"Strong Right\", \"Left\", \"Strong Left\" for evasion direction. The minimum score will correspond to the decision taken.\n",
    "\n",
    "![acas](imgs/acas.png)\n",
    "\n",
    "The network takes 5 inputs:\n",
    "- ρ (m): Distance from ownship to intruder.\n",
    "- θ (rad): Angle to intruder relative to ownship heading\n",
    "direction.\n",
    "- ψ (rad): Heading angle of intruder relative to ownship\n",
    "heading direction.\n",
    "- $v_{own}$ (m/s): Speed of ownship.\n",
    "- $v_{int}$ (m/s) Speed of intruder.\n",
    "\n",
    "We want to prove one property on this network: \n",
    "\n",
    "**Property 1.**\n",
    "- Description: If the intruder is near and approaching from the left, the network advises \"strong right\".\n",
    "- Input constraints:\n",
    "  - $250 ≤ ρ ≤ 400$,\n",
    "  - $0.2 ≤ \\theta ≤ 0.3$,\n",
    "  - $-3.141592 ≤ \\psi ≤ -3.136592$ ,\n",
    "  - $250 ≤ v_{own} ≤ 400$,\n",
    "  - $50 ≤ v_{int} ≤ 200$.\n",
    "- Desired output property: the score for \"strong right\" is minimal (\"strong right\" is output[4]) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 1: Read the network & define property\n",
    "\n",
    "The model is saved in a basic neural network format called NNet and is called `acas.nnet`. A loader has already been written in the `NNet.py` file so that it can be read and executed on a numpy array. \n",
    "\n",
    "The NNet format also includes a rescaling on the inputs and outputs of the network."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from NNet import NNetLoader\n",
    "import numpy as np\n",
    "\n",
    "inputs = np.array([0., 0., 0., 0., 0.])\n",
    "model = NNetLoader(\"acas.nnet\")\n",
    "pred = model.run_NNet(inputs)\n",
    "print(pred)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can have a look at the `run_NNet` function, where some specific checks have been added so that it can work with Intervals. As such, the following code should work directly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "interval_input = np.array([Interval(0., 0.),Interval(0., 0.),Interval(0., 0.),Interval(0., 0.),Interval(0., 0.)])\n",
    "\n",
    "res = model.run_NNet(interval_input)\n",
    "print(res)\n",
    "for i in range(len(res)):\n",
    "    assert isinstance(res[i], Interval)\n",
    "    assert res[i].lower - pred[i] < 1e-10 and res[i].upper - pred[i] < 1e-10\n",
    "\n",
    "print(\"Your intervals work on the NNet file!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create a function to check the property on the output\n",
    "You must check that \"strong right\" is always the lowest score. This involves comparison between Interval so be careful!\n",
    "There should be three possible results for this function:\n",
    "  - \"True\" if the property holds\n",
    "  - \"False\" if you can prove the opposite of the property\n",
    "  - \"Unknown\" if you cannot conclude. For example, [3, 5] <= [4, 6] is \"Unknown\" because we cannot say for sure that it is always greater."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def check_property_1(output: List[Interval]):\n",
    "    raise NotImplementedError"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With this we can now check the property 1 by defining our inputs and running "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "property_input = np.array([]) # fill in with property 1 input intervals\n",
    "\n",
    "res = model.run_NNet(property_input)\n",
    "print(res)\n",
    "\n",
    "check_property_1(res)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Surprisingly (or not), we fail to prove the result. You can see that the output intervals are very large, this is due to the imprecision of the Interval domain: if $x_1 = [-1, 1]$ then $x_1 - x_1$ = $[-1, 1] - [-1, 1]$ = $[-2, 2]$ when only using the Intervals!\n",
    "\n",
    "As we are not keeping tracks of the variables, we lose a lot of the relations between variables and the imprecision in the analysis keeps piling increasing!\n",
    "\n",
    "## Step 2: Zonotopes\n",
    "\n",
    "Now is the time to create your second abstract domain representing Zonotopes (remember previous lesson or slides are in the `doc` folder)! You should create a Python class that allows to represent each variable as an affine form of the inputs: $v = 3 * x_1 + 2 * x_2\\, +\\, ...$\n",
    "\n",
    "It must implement a function to transform the Zonotope into an Interval. \n",
    "\n",
    "The `relu` function will be implemented __after__ testing the arithmetic of the Zonotope.\n",
    "\n",
    "First, we create a function to create the generators and bias of the Zonotope then we can create the Zonotope class. The generators will be implemented as a dictionnary associating a value to each noise symbols with each noise symbol being identified by a string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def zono_from_interval(itv: Interval, name: str= \"e0\") -> Zonotope:\n",
    "    \"\"\"Return a Zonotope representing exactly the given Interval\"\"\"\n",
    "    raise NotImplementedError\n",
    "\n",
    "class Zonotope:\n",
    "    def __init__(self, generators, bias):\n",
    "        raise NotImplementedError\n",
    "    \n",
    "    def __neg__(self):\n",
    "        raise NotImplementedError\n",
    "        \n",
    "    def __add__(self, other):\n",
    "        raise NotImplementedError\n",
    "    \n",
    "    def __radd__(self, other):\n",
    "        return self.__add__(other)\n",
    "        \n",
    "    def __sub__(self, other):\n",
    "        raise NotImplementedError\n",
    "    \n",
    "    def __mul__(self, other):\n",
    "        if isinstance(other, Zonotope):\n",
    "            raise TypeError\n",
    "        raise NotImplementedError\n",
    "    \n",
    "    def __rmul__(self, other):\n",
    "        return self.__mul__(other)\n",
    "\n",
    "    def __truediv__(self, other):\n",
    "        if isinstance(other, Zonotope):\n",
    "            raise TypeError\n",
    "        return self.__mul__(1 / other)\n",
    "\n",
    "    def to_interval(self) -> Interval:\n",
    "        raise NotImplementedError\n",
    "                 \n",
    "    def __str__(self):\n",
    "        raise NotImplementedError\n",
    "\n",
    "    def __repr__(self):\n",
    "        return self.__str__()\n",
    "\n",
    "#     def relu(self):\n",
    "#         # TODO later: see explanation below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def test_zonotope():\n",
    "    itv = Interval(-1, 2)\n",
    "    assert zono_from_interval(itv).to_interval() == itv\n",
    "    \n",
    "    z1 = zono_from_interval(Interval(-1, 1), \"e1\")\n",
    "    z2 = zono_from_interval(Interval(0, 2), \"e2\")\n",
    "\n",
    "    assert (z1 + z2).to_interval() == Interval(-1, 3)\n",
    "    assert (z1 + 3).to_interval() == Interval(2, 4)\n",
    "    assert (-z2).to_interval() == Interval(-2, 0)\n",
    "    assert (z1 - z1).to_interval() == Interval(0, 0)\n",
    "    assert (.5*z2).to_interval() == Interval(0, 1)\n",
    "    assert (z1 + 1).to_interval() == Interval(0, 2)\n",
    "    z3 = zono_from_interval(Interval(-1, 1), \"e3\")\n",
    "    assert (z1 - z3).to_interval() != Interval(0, 0)\n",
    "    print(\"Your arithmetic on the Zonotope domain is working!\")\n",
    "\n",
    "test_zonotope()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will also implement a `relu` function in your Zonotope class as this will be useful in our network. As the relu function is not linear, we will have to do an approximation of it to implement it! Two solutions are possible:\n",
    "- Restart your Zonotope from scratch at each ReLU, i.e. you take the current intervals and just says this is a new 'input' and continue like this\n",
    "- Create a linear overapproximation of your ReLU (see previous lesson or ask the teacher)\n",
    "\n",
    "To test what you implemented, set the parameter `method` below to either `\"scratch\"` or `\"linear_approx\"`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def test_relu_zonotope(method: str):\n",
    "    z1 = zono_from_interval(Interval(1, 2))\n",
    "    z2 = zono_from_interval(Interval(-2, -1))\n",
    "    z3 = zono_from_interval(Interval(-3, 1))\n",
    "\n",
    "    assert z1.relu().to_interval() == Interval(1, 2)\n",
    "    assert z2.relu().to_interval() == Interval(0, 0)\n",
    "\n",
    "    # if you implemented relu with the restarting from scratch\n",
    "    if method == \"scratch\":\n",
    "        assert z3.relu().to_interval() == Interval(0.0, 1.0), \\\n",
    "            f\"{z3.relu().to_interval()} differs from {Interval(0.0, 1.0)}\"\n",
    "\n",
    "    # if you implemented relu with linear approximation\n",
    "    elif method == \"linear_approx\":\n",
    "        rz3 = z3.relu()\n",
    "        assert rz3.to_interval() == Interval(-0.75, 1.0), \\\n",
    "            f\"{rz3.to_interval()} differs from {Interval(-0.75, 1.0)}\"\n",
    "        assert (rz3 - .25 * z3).to_interval() == Interval(0.0, .75), \\\n",
    "            f\"{(rz3 - .25*z3).to_interval()} differs from {Interval(0.0, 0.75)}\"\n",
    "    else:\n",
    "        print(f\"The string '{method}' is not equal to 'scratch' or 'linear_approx'\")\n",
    "        raise ValueError\n",
    "    print(\"Your Zonotope correctly approximate the ReLU! :)\")\n",
    "\n",
    "method = \"Your Method\"\n",
    "test_relu_zonotope(method)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With your Zonotopes working you should be able to launch the `run_NNet` function on an array of Zonotopes you created and check the property again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "property_input = np.array([])\n",
    "\n",
    "res = model.run_NNet(property_input)\n",
    "print(res)\n",
    "\n",
    "check_property_1(res)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Is it better? Can you prove the property? \n",
    "\n",
    "Compare both possibilities for the ReLU, which one gives the best result ?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Bonus step**: Combine Intervals and Zonotopes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 3: More analysis ?\n",
    "\n",
    "What can we do to prove our property if Zonotopes are not enough? \n",
    "\n",
    "A first possibility would be to use an even more precise domain. There exists a lot of different abstract domains that could be used here. Some of them such as Hybrid Zonotopes or Tropical Polyhedras can exactly represent a ReLU and thus do not introduce any error in computation! However, generally each of these domains present their own tradeoff: Hybrid Zonotopes are very expensive to compute and Tropical Polyhedras need to surapproximate the addition. \n",
    "You can try and come up with your own domains and abstractions to see if you can do better!\n",
    "\n",
    "A second possibility is to do more analysis. Easy, right? If you divide your space into smaller bits your analysis will get more precise. This can be done at two levels:\n",
    "- Splitting the ReLU function between positive and negative values so that it remains linear in both cases\n",
    "- Splitting the input space into smaller parts\n",
    "\n",
    "Proving the property on all sub space you create means that the property will be proven on the whole space. This means that eventually you will prove it. This is what we will try to do now by splitting the input space.\n",
    "\n",
    "\n",
    "#### Divide inputs\n",
    "\n",
    "You can start by creating function to divide an interval in two and then an input."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "def divide_interval(x: Interval) -> (Interval, Interval):\n",
    "    raise NotImplementedError\n",
    "\n",
    "assert divide_interval(Interval(0, 1)) == (Interval(0, 0.5), Interval(0.5, 1))\n",
    "assert divide_interval(Interval(-5, 1)) == (Interval(-5, -2), Interval(-2, 1))\n",
    "\n",
    "def divide_input(x: np.ndarray, axis: int) -> (np.ndarray, np.ndarray):\n",
    "    raise NotImplementedError\n",
    "\n",
    "div = divide_input(np.array([Interval(0, 1), Interval(-2, 3)]), 1)\n",
    "assert np.all(div[0] == np.array([Interval(0, 1), Interval(-2, 0.5)]))\n",
    "assert np.all(div[1] == np.array([Interval(0, 1), Interval(0.5, 3)]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Iteration algorithm\n",
    "\n",
    "Now we can divide our inputs, we can start working on an algorithm to prove our property. The general idea of the algorithm would be:\n",
    "\n",
    "1. Initial: 1 Interval per input as defined by the property\n",
    "2. Run an analysis with `run_NNet`\n",
    "3. Verify the property\n",
    "4. If property is verified stop. \n",
    "   Otherwise, divide an interval in 2 and come back to step 2 for both subspaces created\n",
    "5. If all subspaces are proven True, we can conclude that the initial space is as well.\n",
    "\n",
    "**Bonus**: You can use something like the `tqdm` package for your main loop to visualise the progress"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def analyse(inputs: np.ndarray):\n",
    "    raise NotImplementedError\n",
    "\n",
    "initial = np.array([])\n",
    "analyse(initial)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can print some statistic like the number of analysis you do or the time taken. \n",
    "\n",
    "**Can you do less analysis than PyRAT?**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyrat_api import launch_pyrat\n",
    "\n",
    "launch_pyrat(\"acas.nnet\", \"acas_property_1.txt\", domains=[\"zonotopes\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Heuristics to go further\n",
    "\n",
    "How can we reach the same number of analysis (or lower) as PyRAT:\n",
    "- Selecting the interval to split more carefully\n",
    "- Ignoring some of the preliminary analysis because we know they won't succeed?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*****\n",
    "\n",
    "## Part 3: Choosing a network for image classification\n",
    "\n",
    "For this section we will focus on an open source image dataset for classification purpose. The images we will use are a subset of the Fashion Mnist dataset ([available here](https://www.kaggle.com/datasets/zalando-research/fashionmnist)). This dataset was developed to replace the MNIST dataset which was overused and presents the same caracteristics:\n",
    "- 28x28 grayscales images\n",
    "- 10 output classes: T-shirt, Trouser, Pullover, Dress, Coat, Sandal, Shirt, Sneaker, Bag, Ankle boot\n",
    "\n",
    "![fmnist](imgs/fashion_mnist.png)\n",
    "\n",
    "We will use a subset of 50 images for the purpose of this TP.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The goal for this part is to decide which model would be best suited for our needs. Let's suppose we are in a critical system where picking the right class for the cloth might lead to any potential damage (ecological, financial, loss of clients, ..). \n",
    "\n",
    "We propose 5 models trained with different methods:\n",
    "1. **Baseline model**, normal training\n",
    "2. **Adversarial model**, adversarial training\n",
    "3. **Pruned model, normal** training + pruning\n",
    "4. **Certified model**, certified training\n",
    "5. **Pruned certified model**, certified training + pruning\n",
    "\n",
    "We must decide on a model to use. The accuracy of the models is already calculated on the whole test set (more than 50 images). This is already a first criteria of choice as some training lead to less accuracy.\n",
    "\n",
    "|Model | Accuracy|\n",
    "|--- | ---|\n",
    "|Baseline | 90.50%|\n",
    "|Adversarial | 79%|\n",
    "|Pruned | 89%|\n",
    "|Certified | 72.30%|\n",
    "|Pruned Certified | 73.20%|\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 1: Local robustness \n",
    "\n",
    "We will look at the local robustness of the five models around the 50 images from our subdataset. All models and images are  available in the `fmnist` folder. We already created two utility functions to read images and to launch PyRAT on an image and a network. `read_images` return a list of `(image_i, label_i)` while `local_robustness` returns the robustness of a network around an image for a given perturbation.\n",
    "\n",
    "An example is given below with a sneaker image and a perturbation of 1/255 (1 pixel modified)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyrat_api import read_images, local_robustness\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "\n",
    "images = read_images()\n",
    "image_0, label_0 = images[0]\n",
    "print(f\"Image shape: {image_0.shape}, label: {label_0}\")\n",
    "plt.imshow(image_0, cmap='gray')\n",
    "\n",
    "res, elapsed = local_robustness(model_path=\"fmnist/baseline.onnx\", image=image_0, label=label_0, pert=1/255, domains=[\"zonotopes\"])\n",
    "print(f\"Robust: {res}, Time: {elapsed}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now its your turn create a function that analyse all the given images for a given network and a given perturbation and returns a robustness score including safe images, unknown images and unsafe images as well as a mean time for the analysis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def robustness(images, model_path, pert):\n",
    "    raise NotImplementedError\n",
    "    \n",
    "robustness(images, \"fmnist/baseline.onnx\", 1/255) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With these results you can now try and plot the results to show the evolution in function of the level of intensity for the perturbation. We aim to see if a model is more robust than another to what could be adversarial perturbation. You can use matplotlib to plot these results. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_robustness():\n",
    "    raise NotImplementedError"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 2: Metamorphic testing\n",
    "\n",
    "We checked with the engineer in charge of taking pictures of the clothes on the condition in which he takes pictures. After a careful investigation we realised that the following issues might be happening in our picture:\n",
    "- Luminosity of the setting varies from -30 to +30\n",
    "- Angle of the clothes might vary from -15° to + 15°\n",
    "- Picture can be blurry \n",
    "\n",
    "In that sense, to test the robustness of our model we will proceed to see if it is sensitive to these perturbations. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To apply these transformation and compute the stability of a network towards we will use a tool developed at CEA called AIMOS which does exactly that.\n",
    "\n",
    "We will use the online version of AIMOS to test the stability of the networks. You can access it [here](https://caisar-platform.com/aimos-demo/) or use the embedded version below. The perturbation are already defined and you can use them directly.\n",
    "\n",
    "Try to apply the different perturbation on the different models and images available in the fmnist folder. **What do you notice?**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%html\n",
    "<iframe src=\"https://caisar-platform.com/aimos-demo/\" width=\"1200\" height=\"1000\"></iframe>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 3: Choose a model\n",
    "\n",
    "**With all of these results, what model would you choose, why?**"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
