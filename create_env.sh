#!/bin/bash

pip install virtualenv
virtualenv .virtualenvs/tutorial
source .virtualenvs/tutorial/bin/activate
pip install -r /home/tutorial/code/requirements.txt