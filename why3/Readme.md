# Verification of interval and zonotopes propagation

The file [interval_template.mlw](./interval_template.mlw) gives the template for the proof of the propagation of the domain of interval and zonotope for the addition and the relu operation (`max(0,x)`).

The proof can be attempted using the javascript version of the tool on [try why3](https://www.why3.org/try/?lang=whyml) (except for the last task), or by installing why3 (
[Opam package](https://opam.ocaml.org/packages/why3/),
[Debian package](https://packages.debian.org/stable/why3),
[Fedora package](https://packages.fedoraproject.org/pkgs/why3/why3/),
[Ubuntu package](https://packages.ubuntu.com/focal/why3))

The goal is to prove some operations implemented for the verification of neural networks, [tutorial.ipynb](../code/tutorial.ipynb)

## Why3

    * `/\`: and; `\/`: or; `->`: implication
    * `+,*,/,-`: real operations obtained from `use real.Real`


